﻿using System;
using RoccatTalk;

namespace RoccatTalkSimple
{
    /// <summary>
    /// Example Program for the RoccatTalk C# SDK
    /// </summary>
    class Program
    {   
        static void Main(string[] args)
        {
            RYOSFX RCT = new RYOSFX(LAYOUT.GER);
            try
            {
                //Connect to the Keyboard
                RCT.InitRYOSFX();

                Console.WriteLine("Set SDKMode ON");
                RCT.SDKMode(true);

                Console.WriteLine("Set SpaceBar RED");
                Console.WriteLine("Set WinKey GREEN");
                RCT.SetKey(KEYSGER.LEER, 255, 0, 0);
                RCT.SetKey(KEYSGER.WIN, 0, 255, 0);

                Console.WriteLine("Set SpaceBar ON");
                Console.WriteLine("Set WinKey ON");
                RCT.SetKey(KEYSGER.LEER, KEYSTATE.ON);
                RCT.SetKey(KEYSGER.WIN, KEYSTATE.ON);

                Console.WriteLine("Send Frame");
                RCT.SendFrame();

                Console.ReadLine();


                Console.WriteLine("Set SpaceBar OFF");
                Console.WriteLine("Set WinKey OFF");
                RCT.SetKey(KEYSGER.LEER, KEYSTATE.OFF);
                RCT.SetKey(KEYSGER.WIN, KEYSTATE.OFF);

                Console.WriteLine("Send Frame");
                RCT.SendFrame();

                Console.ReadLine();

                Console.WriteLine("Wipe BLUE");
                RCT.SetColorAll(0, 0, 255);

                Console.WriteLine("Send Frame");
                RCT.SendFrame();

                Console.ReadLine();


                

                Console.WriteLine("Slow Wipe RED (ESC-Pause)");
                for (int i = 0; i < 16; i++)
                {                    
                    Console.WriteLine("Set Key" + i + " 255,0,0 ON"); 
                    RCT.SetKey(i, 255, 0, 0, KEYSTATE.ON);

                    Console.WriteLine("Send Frame\n");
                    RCT.SendFrame();
                    
                    System.Threading.Thread.Sleep(20);
                }

                Console.ReadLine();
            }
            finally
            {
                Console.WriteLine("Set SDKMode OFF");

                RCT.SDKMode(false);
            }         
        }
    }
}
