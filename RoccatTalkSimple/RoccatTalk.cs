﻿using System;
using System.Runtime.InteropServices;

namespace RoccatTalk
{
    #region KEYBOARDS
    /// <summary>
    /// Class for the Roccat RYOS MK FX
    /// </summary>
    public class RYOSFX : DLLImport, IFrameModell
    {
        private const int FRAMEONOFF = 0;
        private const int FRAMERED = 1;
        private const int FRAMEGREEN = 2;
        private const int FRAMEBLUE = 3;
        private const int MAXCOLOR = 255;
        private const int MINCOLOR = 0;

        private byte layout;
        private byte[,] frame;

        #region CONSTRUCTOR
        public RYOSFX() : base()
        {
            initFrame();
        }
        public RYOSFX(LAYOUT layout)
        {
            this.layout = Convert.ToByte(layout);
            initFrame();
        }
        public RYOSFX(byte layout)
        {
            this.layout = layout;
            initFrame();
        }
        #endregion

        #region INIT
               
        public void initFrame()
        {
            this.frame = new byte[4, NUMKEYS];

            for (int i = 0; i < 4; i++)
            {
                for (int z = 0; z < NUMKEYS; z++)
                {
                    if (i == 0)
                    {
                        this.frame[i, z] = 1;
                    }
                    else
                    {
                        this.frame[i, z] = 0;
                    }

                }
            }
        }

        /// <summary>
        /// Establishes connection to Roccat RYOS MK FX
        /// </summary>
        /// <returns>False: when connection failed / True: when connection has been established</returns>
        public bool InitRYOSFX()
        {
            return init_ryos_talk();
        }

        /// <summary>
        /// Turn on or off SDK-Mode
        /// </summary>
        /// <param name="enable">SDK-Mode (True=ON/False=OFF)</param>
        public void SDKMode(bool enable)
        {
            try
            {
                set_ryos_kb_SDKmode(enable);
            }
            catch (Exception e)
            {
                throw new FailedToEnterSDKModeException("Failed to enter SDK Mode", e);
            }

        }

        #endregion

        #region FRAMEMODEL
        /// <summary>
        /// Sends the currently stored frame to the Roccat RYOS MK FX Keyboard
        /// </summary>
        public void SendFrame()
        {
            try
            {
                byte[] FrameS = new byte[NUMKEYS];
                byte[] FrameR = new byte[NUMKEYS];
                byte[] FrameG = new byte[NUMKEYS];
                byte[] FrameB = new byte[NUMKEYS];

                for (int i = 0; i < NUMKEYS; i++)
                {
                    FrameS[i] = frame[FRAMEONOFF, i];
                    FrameR[i] = frame[FRAMERED, i];
                    FrameG[i] = frame[FRAMEGREEN, i];
                    FrameB[i] = frame[FRAMEBLUE, i];
                }

                Set_all_LEDSFX(FrameS, FrameR, FrameG, FrameB, this.layout);
            }
            catch (Exception e)
            {
                throw new FailedToSendFrameException(frame, "Failed to Send Frame", e);
            }
        }

        /// <summary>
        /// Returns a copy of the current Framedata
        /// </summary>
        /// <returns>copy of Frame as int[,]</returns>
        public int[,] GetFrameInt()
        {
            int[,] result = new int[4, NUMKEYS];

            for (int x = 0; x<4;x++)
            {
                for (int i = 0; i < NUMKEYS; i++)
                {
                    if (x == FRAMEONOFF)
                    {
                        if (this.frame[x,i] == 0x00)
                        {
                            result[x, i] = 0;
                        }
                        else
                        {
                            result[x, i] = 1;
                        }                        
                    }
                    else
                    {
                        result[x, i] = Convert.ToInt32(frame[x, i]);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a copy of the current Framedata
        /// </summary>
        /// <returns>copy of Frame as byte[,]</returns>
        public byte[,] GetFrameByte()
        {
            byte[,] result = new byte[4, NUMKEYS];

            for (int x = 0; x < 4; x++)
            {
                for (int i = 0; i < NUMKEYS; i++)
                {
                    result[x, i] = Convert.ToByte(frame[x, i]);
                }
            }
            return result;
        }

        /// <summary>
        /// Sets the current Frame
        /// </summary>
        /// <param name="frame">Frame-Data as int[(OnOff,R,G,B),keypos]</param>
        public void SetFrame(int[,] frame)
        {
            if (frame.Length > 4 * NUMKEYS)
            {
                throw new InvalidFrameException("The Frame is to big. Size: "+frame.Length+" (Expcected: 4*"+NUMKEYS+"="+(4*NUMKEYS)+")");
            }
            else
            {
                bool frameok = true;
               for (int i = 0; i < NUMKEYS; i++)
                {
                    for (int x = 0; x < 4; x++)
                    {
                        if (x == 0)
                        {
                           if ((frame[x,i] > 1) || (frame[x,i] < 0))
                           { 
                                frameok = false;
                           }
                        }
                        else
                        {
                            if ((frame[x,i] > MAXCOLOR) || (frame[x,i] < MINCOLOR))
                            {
                                frameok = false;
                            }
                        }
                    }
                }

               if (frameok)
               {
                    for (int i = 0; i < NUMKEYS; i++)
                    {
                        for (int x = 0; x < 4; x++)
                        {
                            if (x == 0)
                            {
                                if (frame[x, i] == 0)
                                {
                                    this.frame[x, i] = 0x00;
                                }
                                else
                                {
                                    this.frame[x, i] = 0x01;
                                }
                            }
                            else
                            {
                                this.frame[x, i] = Convert.ToByte(frame[x, i]);
                            }
                        }
                    }
                }
               else
               {
                    throw new InvalidFrameException("The Frame contains unexpected values");
               }
            }
        }

        /// <summary>
        /// Sets the Frame-Data by the pixels of a bitmap
        /// </summary>
        /// <param name="image">a 22x6 image (automatic downscale to 22x6)</param>
        public void SetFrame(System.Drawing.Bitmap image)
        {
            System.Drawing.Bitmap frameimg = new System.Drawing.Bitmap(image, 22, 6);          
            
            if (layout == 0x00)
            {


                //Line 1
                SetKey(0, frameimg.GetPixel(1, 0).R, frameimg.GetPixel(1, 0).G, frameimg.GetPixel(1, 0).B, true);
                SetKey(1, frameimg.GetPixel(3, 0).R, frameimg.GetPixel(3, 0).G, frameimg.GetPixel(3, 0).B, true);
                SetKey(2, frameimg.GetPixel(4, 0).R, frameimg.GetPixel(4, 0).G, frameimg.GetPixel(4, 0).B, true);
                SetKey(3, frameimg.GetPixel(5, 0).R, frameimg.GetPixel(5, 0).G, frameimg.GetPixel(5, 0).B, true);
                SetKey(4, frameimg.GetPixel(6, 0).R, frameimg.GetPixel(6, 0).G, frameimg.GetPixel(6, 0).B, true);
                SetKey(5, frameimg.GetPixel(7, 0).R, frameimg.GetPixel(7, 0).G, frameimg.GetPixel(7, 0).B, true);
                SetKey(6, frameimg.GetPixel(8, 0).R, frameimg.GetPixel(8, 0).G, frameimg.GetPixel(8, 0).B, true);
                SetKey(7, frameimg.GetPixel(9, 0).R, frameimg.GetPixel(9, 0).G, frameimg.GetPixel(9, 0).B, true);
                SetKey(8, frameimg.GetPixel(10, 0).R, frameimg.GetPixel(10, 0).G, frameimg.GetPixel(10, 0).B, true);
                SetKey(9, frameimg.GetPixel(11, 0).R, frameimg.GetPixel(11, 0).G, frameimg.GetPixel(11, 0).B, true);
                SetKey(10, frameimg.GetPixel(12, 0).R, frameimg.GetPixel(12, 0).G, frameimg.GetPixel(12, 0).B, true);
                SetKey(11, frameimg.GetPixel(13, 0).R, frameimg.GetPixel(13, 0).G, frameimg.GetPixel(13, 0).B, true);
                SetKey(12, frameimg.GetPixel(14, 0).R, frameimg.GetPixel(14, 0).G, frameimg.GetPixel(14, 0).B, true);
                SetKey(13, frameimg.GetPixel(15, 0).R, frameimg.GetPixel(15, 0).G, frameimg.GetPixel(15, 0).B, true);
                SetKey(14, frameimg.GetPixel(16, 0).R, frameimg.GetPixel(16, 0).G, frameimg.GetPixel(16, 0).B, true);
                SetKey(15, frameimg.GetPixel(17, 0).R, frameimg.GetPixel(17, 0).G, frameimg.GetPixel(17, 0).B, true);

                //Line 2
                SetKey(16, frameimg.GetPixel(0, 1).R, frameimg.GetPixel(0, 1).G, frameimg.GetPixel(0, 1).B, true);
                SetKey(17, frameimg.GetPixel(1, 1).R, frameimg.GetPixel(1, 1).G, frameimg.GetPixel(1, 1).B, true);
                SetKey(18, frameimg.GetPixel(2, 1).R, frameimg.GetPixel(2, 1).G, frameimg.GetPixel(2, 1).B, true);
                SetKey(19, frameimg.GetPixel(3, 1).R, frameimg.GetPixel(3, 1).G, frameimg.GetPixel(3, 1).B, true);
                SetKey(20, frameimg.GetPixel(4, 1).R, frameimg.GetPixel(4, 1).G, frameimg.GetPixel(4, 1).B, true);
                SetKey(21, frameimg.GetPixel(5, 1).R, frameimg.GetPixel(5, 1).G, frameimg.GetPixel(5, 1).B, true);
                SetKey(22, frameimg.GetPixel(6, 1).R, frameimg.GetPixel(6, 1).G, frameimg.GetPixel(6, 1).B, true);
                SetKey(23, frameimg.GetPixel(7, 1).R, frameimg.GetPixel(7, 1).G, frameimg.GetPixel(7, 1).B, true);
                SetKey(24, frameimg.GetPixel(8, 1).R, frameimg.GetPixel(8, 1).G, frameimg.GetPixel(8, 1).B, true);
                SetKey(25, frameimg.GetPixel(9, 1).R, frameimg.GetPixel(9, 1).G, frameimg.GetPixel(9, 1).B, true);
                SetKey(26, frameimg.GetPixel(10, 1).R, frameimg.GetPixel(10, 1).G, frameimg.GetPixel(10, 1).B, true);
                SetKey(27, frameimg.GetPixel(11, 1).R, frameimg.GetPixel(11, 1).G, frameimg.GetPixel(11, 1).B, true);
                SetKey(28, frameimg.GetPixel(11, 1).R, frameimg.GetPixel(11, 1).G, frameimg.GetPixel(11, 1).B, true);
                SetKey(29, frameimg.GetPixel(12, 1).R, frameimg.GetPixel(12, 1).G, frameimg.GetPixel(12, 1).B, true);
                SetKey(30, frameimg.GetPixel(14, 1).R, frameimg.GetPixel(14, 1).G, frameimg.GetPixel(14, 1).B, true);
                SetKey(31, frameimg.GetPixel(15, 1).R, frameimg.GetPixel(15, 1).G, frameimg.GetPixel(15, 1).B, true);
                SetKey(32, frameimg.GetPixel(16, 1).R, frameimg.GetPixel(16, 1).G, frameimg.GetPixel(16, 1).B, true);
                SetKey(33, frameimg.GetPixel(17, 1).R, frameimg.GetPixel(17, 1).G, frameimg.GetPixel(17, 1).B, true);
                SetKey(34, frameimg.GetPixel(18, 1).R, frameimg.GetPixel(18, 1).G, frameimg.GetPixel(18, 1).B, true);
                SetKey(35, frameimg.GetPixel(19, 1).R, frameimg.GetPixel(19, 1).G, frameimg.GetPixel(19, 1).B, true);
                SetKey(36, frameimg.GetPixel(20, 1).R, frameimg.GetPixel(20, 1).G, frameimg.GetPixel(20, 1).B, true);
                SetKey(37, frameimg.GetPixel(21, 1).R, frameimg.GetPixel(21, 1).G, frameimg.GetPixel(21, 1).B, true);

                //Line 3
                SetKey(38, frameimg.GetPixel(0, 2).R, frameimg.GetPixel(0, 2).G, frameimg.GetPixel(0, 2).B, true);
                SetKey(39, frameimg.GetPixel(1, 2).R, frameimg.GetPixel(1, 2).G, frameimg.GetPixel(1, 2).B, true);
                SetKey(40, frameimg.GetPixel(2, 2).R, frameimg.GetPixel(2, 2).G, frameimg.GetPixel(2, 2).B, true);
                SetKey(41, frameimg.GetPixel(3, 2).R, frameimg.GetPixel(3, 2).G, frameimg.GetPixel(3, 2).B, true);
                SetKey(42, frameimg.GetPixel(4, 2).R, frameimg.GetPixel(4, 2).G, frameimg.GetPixel(4, 2).B, true);
                SetKey(43, frameimg.GetPixel(5, 2).R, frameimg.GetPixel(5, 2).G, frameimg.GetPixel(5, 2).B, true);
                SetKey(44, frameimg.GetPixel(6, 2).R, frameimg.GetPixel(6, 2).G, frameimg.GetPixel(6, 2).B, true);
                SetKey(45, frameimg.GetPixel(7, 2).R, frameimg.GetPixel(7, 2).G, frameimg.GetPixel(7, 2).B, true);
                SetKey(46, frameimg.GetPixel(8, 2).R, frameimg.GetPixel(8, 2).G, frameimg.GetPixel(8, 2).B, true);
                SetKey(47, frameimg.GetPixel(9, 2).R, frameimg.GetPixel(9, 2).G, frameimg.GetPixel(9, 2).B, true);
                SetKey(48, frameimg.GetPixel(10, 2).R, frameimg.GetPixel(10, 2).G, frameimg.GetPixel(10, 2).B, true);
                SetKey(49, frameimg.GetPixel(11, 2).R, frameimg.GetPixel(11, 2).G, frameimg.GetPixel(11, 2).B, true);
                SetKey(50, frameimg.GetPixel(11, 2).R, frameimg.GetPixel(11, 2).G, frameimg.GetPixel(11, 2).B, true);
                SetKey(51, frameimg.GetPixel(12, 2).R, frameimg.GetPixel(12, 2).G, frameimg.GetPixel(12, 2).B, true);
                SetKey(52, frameimg.GetPixel(14, 2).R, frameimg.GetPixel(14, 2).G, frameimg.GetPixel(14, 2).B, true);
                SetKey(53, frameimg.GetPixel(15, 2).R, frameimg.GetPixel(15, 2).G, frameimg.GetPixel(15, 2).B, true);
                SetKey(54, frameimg.GetPixel(16, 2).R, frameimg.GetPixel(16, 2).G, frameimg.GetPixel(16, 2).B, true);
                SetKey(55, frameimg.GetPixel(17, 2).R, frameimg.GetPixel(17, 2).G, frameimg.GetPixel(17, 2).B, true);
                SetKey(56, frameimg.GetPixel(18, 2).R, frameimg.GetPixel(18, 2).G, frameimg.GetPixel(18, 2).B, true);
                SetKey(57, frameimg.GetPixel(19, 2).R, frameimg.GetPixel(19, 2).G, frameimg.GetPixel(19, 2).B, true);
                SetKey(58, frameimg.GetPixel(20, 2).R, frameimg.GetPixel(20, 2).G, frameimg.GetPixel(20, 2).B, true);
                SetKey(59, frameimg.GetPixel(21, 2).R, frameimg.GetPixel(21, 2).G, frameimg.GetPixel(21, 2).B, true);

                //Line 4
                SetKey(60, frameimg.GetPixel(0, 3).R, frameimg.GetPixel(0, 3).G, frameimg.GetPixel(0, 3).B, true);
                SetKey(61, frameimg.GetPixel(1, 3).R, frameimg.GetPixel(1, 3).G, frameimg.GetPixel(1, 3).B, true);
                SetKey(62, frameimg.GetPixel(2, 3).R, frameimg.GetPixel(2, 3).G, frameimg.GetPixel(2, 3).B, true);
                SetKey(63, frameimg.GetPixel(3, 3).R, frameimg.GetPixel(3, 3).G, frameimg.GetPixel(3, 3).B, true);
                SetKey(64, frameimg.GetPixel(4, 3).R, frameimg.GetPixel(4, 3).G, frameimg.GetPixel(4, 3).B, true);
                SetKey(65, frameimg.GetPixel(5, 3).R, frameimg.GetPixel(5, 3).G, frameimg.GetPixel(5, 3).B, true);
                SetKey(66, frameimg.GetPixel(6, 3).R, frameimg.GetPixel(6, 3).G, frameimg.GetPixel(6, 3).B, true);
                SetKey(67, frameimg.GetPixel(7, 3).R, frameimg.GetPixel(7, 3).G, frameimg.GetPixel(7, 3).B, true);
                SetKey(68, frameimg.GetPixel(8, 3).R, frameimg.GetPixel(8, 3).G, frameimg.GetPixel(8, 3).B, true);
                SetKey(69, frameimg.GetPixel(9, 3).R, frameimg.GetPixel(9, 3).G, frameimg.GetPixel(9, 3).B, true);
                SetKey(70, frameimg.GetPixel(10, 3).R, frameimg.GetPixel(10, 3).G, frameimg.GetPixel(10, 3).B, true);
                SetKey(71, frameimg.GetPixel(11, 3).R, frameimg.GetPixel(11, 3).G, frameimg.GetPixel(11, 3).B, true);
                SetKey(72, frameimg.GetPixel(11, 3).R, frameimg.GetPixel(11, 3).G, frameimg.GetPixel(11, 3).B, true);
                SetKey(73, frameimg.GetPixel(13, 3).R, frameimg.GetPixel(13, 3).G, frameimg.GetPixel(13, 3).B, true);
                SetKey(74, frameimg.GetPixel(18, 3).R, frameimg.GetPixel(18, 3).G, frameimg.GetPixel(18, 3).B, true);
                SetKey(75, frameimg.GetPixel(19, 3).R, frameimg.GetPixel(19, 3).G, frameimg.GetPixel(19, 3).B, true);
                SetKey(76, frameimg.GetPixel(20, 3).R, frameimg.GetPixel(20, 3).G, frameimg.GetPixel(20, 3).B, true);

                //Line 5
                SetKey(77, frameimg.GetPixel(0, 4).R, frameimg.GetPixel(0, 4).G, frameimg.GetPixel(0, 4).B, true);
                SetKey(78, frameimg.GetPixel(1, 4).R, frameimg.GetPixel(1, 4).G, frameimg.GetPixel(1, 4).B, true);
                SetKey(79, frameimg.GetPixel(2, 4).R, frameimg.GetPixel(2, 4).G, frameimg.GetPixel(2, 4).B, true);
                SetKey(80, frameimg.GetPixel(3, 4).R, frameimg.GetPixel(3, 4).G, frameimg.GetPixel(3, 4).B, true);
                SetKey(81, frameimg.GetPixel(4, 4).R, frameimg.GetPixel(4, 4).G, frameimg.GetPixel(4, 4).B, true);
                SetKey(82, frameimg.GetPixel(5, 4).R, frameimg.GetPixel(5, 4).G, frameimg.GetPixel(5, 4).B, true);
                SetKey(83, frameimg.GetPixel(6, 4).R, frameimg.GetPixel(6, 4).G, frameimg.GetPixel(6, 4).B, true);
                SetKey(84, frameimg.GetPixel(7, 4).R, frameimg.GetPixel(7, 4).G, frameimg.GetPixel(7, 4).B, true);
                SetKey(85, frameimg.GetPixel(8, 4).R, frameimg.GetPixel(8, 4).G, frameimg.GetPixel(8, 4).B, true);
                SetKey(86, frameimg.GetPixel(9, 4).R, frameimg.GetPixel(9, 4).G, frameimg.GetPixel(9, 4).B, true);
                SetKey(87, frameimg.GetPixel(10, 4).R, frameimg.GetPixel(10, 4).G, frameimg.GetPixel(10, 4).B, true);
                SetKey(88, frameimg.GetPixel(11, 4).R, frameimg.GetPixel(11, 4).G, frameimg.GetPixel(11, 4).B, true);
                SetKey(89, frameimg.GetPixel(11, 4).R, frameimg.GetPixel(11, 4).G, frameimg.GetPixel(11, 4).B, true);
                SetKey(90, frameimg.GetPixel(13, 4).R, frameimg.GetPixel(13, 4).G, frameimg.GetPixel(13, 4).B, true);
                SetKey(91, frameimg.GetPixel(16, 4).R, frameimg.GetPixel(16, 4).G, frameimg.GetPixel(16, 4).B, true);
                SetKey(92, frameimg.GetPixel(18, 4).R, frameimg.GetPixel(18, 4).G, frameimg.GetPixel(18, 4).B, true);
                SetKey(93, frameimg.GetPixel(19, 4).R, frameimg.GetPixel(19, 4).G, frameimg.GetPixel(19, 4).B, true);
                SetKey(94, frameimg.GetPixel(20, 4).R, frameimg.GetPixel(20, 4).G, frameimg.GetPixel(20, 4).B, true);
                SetKey(95, frameimg.GetPixel(21, 4).R, frameimg.GetPixel(21, 4).G, frameimg.GetPixel(21, 4).B, true);

                //Line 6
                SetKey(96, frameimg.GetPixel(0, 5).R, frameimg.GetPixel(0, 5).G, frameimg.GetPixel(0, 5).B, true);
                SetKey(97, frameimg.GetPixel(1, 5).R, frameimg.GetPixel(1, 5).G, frameimg.GetPixel(1, 5).B, true);
                SetKey(98, frameimg.GetPixel(2, 5).R, frameimg.GetPixel(2, 5).G, frameimg.GetPixel(2, 5).B, true);
                SetKey(99, frameimg.GetPixel(3, 5).R, frameimg.GetPixel(3, 5).G, frameimg.GetPixel(3, 5).B, true);
                SetKey(100, frameimg.GetPixel(7, 5).R, frameimg.GetPixel(7, 5).G, frameimg.GetPixel(7, 5).B, true);
                SetKey(101, frameimg.GetPixel(11, 5).R, frameimg.GetPixel(11, 5).G, frameimg.GetPixel(11, 5).B, true);
                SetKey(102, frameimg.GetPixel(11, 5).R, frameimg.GetPixel(11, 5).G, frameimg.GetPixel(11, 5).B, true);
                SetKey(103, frameimg.GetPixel(12, 5).R, frameimg.GetPixel(12, 5).G, frameimg.GetPixel(12, 5).B, true);
                SetKey(104, frameimg.GetPixel(13, 5).R, frameimg.GetPixel(13, 5).G, frameimg.GetPixel(13, 5).B, true);
                SetKey(105, frameimg.GetPixel(15, 5).R, frameimg.GetPixel(15, 5).G, frameimg.GetPixel(15, 5).B, true);
                SetKey(106, frameimg.GetPixel(16, 5).R, frameimg.GetPixel(16, 5).G, frameimg.GetPixel(16, 5).B, true);
                SetKey(107, frameimg.GetPixel(17, 5).R, frameimg.GetPixel(17, 5).G, frameimg.GetPixel(17, 5).B, true);
                SetKey(108, frameimg.GetPixel(18, 5).R, frameimg.GetPixel(18, 5).G, frameimg.GetPixel(18, 5).B, true);
                SetKey(109, frameimg.GetPixel(20, 5).R, frameimg.GetPixel(20, 5).G, frameimg.GetPixel(20, 5).B, true);                
            }
            else
            {

            }
        }


        /// <summary>
        /// Sets color and/or state of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSGER</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetKey(KEYSGER Key, int ColorR, int ColorG, int ColorB, KEYSTATE state)
        {
            SetKey(Convert.ToInt32(Key), ColorR, ColorG, ColorB, state);
        }


        /// <summary>
        /// Sets color and/or state of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSUS</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetKey(KEYSUS Key, int ColorR, int ColorG, int ColorB, KEYSTATE state)
        {
            SetKey(Convert.ToInt32(Key), ColorR, ColorG, ColorB, state);
        }

        /// <summary>
        /// Sets color and/or state of a key
        /// </summary>
        /// <param name="Key">Index of the desired key</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetKey(int Key, int ColorR, int ColorG, int ColorB, KEYSTATE state)
        {
            if (state == KEYSTATE.ON)
            {
                frame[FRAMEONOFF, Key] = 1;
            }
            else
            {
                frame[FRAMEONOFF, Key] = 0;
            }

            frame[FRAMERED, Key] = Convert.ToByte(ColorR);
            frame[FRAMEGREEN, Key] = Convert.ToByte(ColorG);
            frame[FRAMEBLUE, Key] = Convert.ToByte(ColorB);
        }

        /// <summary>
        /// Sets color and/or state of a key
        /// </summary>
        /// <param name="Key">Index of the desired key</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>
        /// <param name="state">Turn key on or off (True/False)</param>
        public void SetKey(int Key, int ColorR, int ColorG, int ColorB, bool state)
        {
            if (state)
            {
                frame[FRAMEONOFF, Key] = 1;
            }
            else
            {
                frame[FRAMEONOFF, Key] = 0;
            }

            frame[FRAMERED, Key] = Convert.ToByte(ColorR);
            frame[FRAMEGREEN, Key] = Convert.ToByte(ColorG);
            frame[FRAMEBLUE, Key] = Convert.ToByte(ColorB);
        }

        /// <summary>
        /// Sets state of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSUS</param>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetKey(KEYSUS Key, KEYSTATE state)
        {
            if (state == KEYSTATE.ON)
            {
                SetKey(Convert.ToInt32(Key), true);
            }
            else
            {
                SetKey(Convert.ToInt32(Key), false);
            }

        }

        /// <summary>
        /// Sets state of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSGER</param>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetKey(KEYSGER Key, KEYSTATE state)
        {
            if (state == KEYSTATE.ON)
            {
                SetKey(Convert.ToInt32(Key), true);
            }
            else
            {
                SetKey(Convert.ToInt32(Key), false);
            }

        }

        /// <summary>
        /// Sets state of a key
        /// </summary>
        /// <param name="Key">Index of desired key</param>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetKey(int Key, KEYSTATE state)
        {
            if (state == KEYSTATE.ON)
            {
                SetKey(Key, true);
            }
            else
            {
                SetKey(Key, false);
            }
        }

        /// <summary>
        /// Sets state of a key
        /// </summary>
        /// <param name="Key">Index of desired key</param>
        /// <param name="state">Turn key on or off (True/False)</param>
        public void SetKey(int Key, bool state)
        {
            if (Key > NUMKEYS || Key < 0)
            {
                throw new KeyIndexOutOfRangeException(Key, "Key Index out of Range: " + Key + " MAX: " + NUMKEYS + " MIN: 0");
            }
            else
            {
                if (state)
                {
                    frame[FRAMEONOFF, Key] = 0x01;
                }
                else
                {
                    frame[FRAMEONOFF, Key] = 0x00;
                }
            }
        }

        /// <summary>
        /// Sets state of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSUS</param>
        /// <param name="state">Turn key on or off (True/False)</param>
        public void SetKey(KEYSUS Key, bool state)
        {
            SetKey(Convert.ToInt32(Key), state);
        }

        /// <summary>
        /// Sets state of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSGER</param>
        /// <param name="state">Turn key on or off (True/False)</param>
        public void SetKey(KEYSGER Key, bool state)
        {
            SetKey(Convert.ToInt32(Key), state);
        }

        /// <summary>
        /// Sets color of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSGER</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>        
        public void SetKey(KEYSGER Key, int ColorR, int ColorG, int ColorB)
        {
            SetKey(Convert.ToInt32(Key), ColorR, ColorG, ColorB);
        }

        /// <summary>
        /// Sets color of a key
        /// </summary>
        /// <param name="Key">Keyindex from RoccatTalk.KEYSUS</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>                
        public void SetKey(KEYSUS Key, int ColorR, int ColorG, int ColorB)
        {
            SetKey(Convert.ToInt32(Key), ColorR, ColorG, ColorB);
        }

        /// <summary>
        /// Sets color of a key
        /// </summary>
        /// <param name="Key">Index of desired key</param>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Blue value from RGB in decimal</param>
        /// <param name="ColorB">Green value from RGB in decimal</param>           
        public void SetKey(int Key, int ColorR, int ColorG, int ColorB)
        {
            if (ColorR > MAXCOLOR || ColorR < MINCOLOR)
            {
                throw new ColorValueOutOfRangeException(ColorR, "Color value out of Range: " + ColorR + " (MAX: " + MAXCOLOR + " MIN: " + MINCOLOR + ")");
            }
            else if (ColorG > MAXCOLOR || ColorG < MINCOLOR)
            {
                throw new ColorValueOutOfRangeException(ColorG, "Color value out of Range: " + ColorG + " (MAX: " + MAXCOLOR + " MIN: " + MINCOLOR + ")");
            }
            else if (ColorB > MAXCOLOR || ColorB < MINCOLOR)
            {
                throw new ColorValueOutOfRangeException(ColorB, "Color value out of Range: " + ColorB + " (MAX: " + MAXCOLOR + " MIN: " + MINCOLOR + ")");
            }
            else if (Key > NUMKEYS || Key < 0)
            {
                throw new KeyIndexOutOfRangeException(Key, "Key Index out of Range: " + Key + " (MAX: " + NUMKEYS + " MIN: 0)");
            }
            else
            {
                frame[FRAMERED, Key] = Convert.ToByte(ColorR);
                frame[FRAMEGREEN, Key] = Convert.ToByte(ColorG);
                frame[FRAMEBLUE, Key] = Convert.ToByte(ColorB);
            }

        }

        

        #endregion

        #region ALL LEDS
        /// <summary>
        /// Colors all of the Keys in a single color and sets their state to ON
        /// </summary>
        /// <param name="ColorR">Red value from RGB in decimal</param>
        /// <param name="ColorG">Green value from RGB in decimal</param>
        /// <param name="ColorB">Blue value from RGB in decimal</param>
        /// <param name="autoTurnOn">Overwrite the keystates to all ON (default TRUE)</param>
        public void SetColorAll(int ColorR, int ColorG, int ColorB,bool autoTurnOn = true)
        {
            if (ColorR > MAXCOLOR || ColorR < MINCOLOR)
            {
                throw new ColorValueOutOfRangeException(ColorR, "Color value out of Range: " + ColorR + " (MAX: " + MAXCOLOR + " MIN: " + MINCOLOR + ")");
            }
            else if (ColorG > MAXCOLOR || ColorG < MINCOLOR)
            {
                throw new ColorValueOutOfRangeException(ColorG, "Color value out of Range: " + ColorG + " (MAX: " + MAXCOLOR + " MIN: " + MINCOLOR + ")");
            }
            else if (ColorB > MAXCOLOR || ColorB < MINCOLOR)
            {
                throw new ColorValueOutOfRangeException(ColorB, "Color value out of Range: " + ColorB + " (MAX: " + MAXCOLOR + " MIN: " + MINCOLOR + ")");
            }
            else
            {
                for (int i = 0; i < NUMKEYS; i++)
                {
                    frame[FRAMERED, i] = Convert.ToByte(ColorR);
                    frame[FRAMEGREEN, i] = Convert.ToByte(ColorG);
                    frame[FRAMEBLUE, i] = Convert.ToByte(ColorB);
                    if (autoTurnOn)
                    {
                        frame[FRAMEONOFF, i] = 0x01;
                    }
                   
                }
            }
        }

        /// <summary>
        /// Turns all keys on or off
        /// </summary>
        /// <param name="state">Keystate from RoccatTalk.KEYSTATE</param>
        public void SetStateAll(KEYSTATE state)
        {
            if (state == KEYSTATE.ON)
            {
                SetStateAll(true);
            }
            else
            {
                SetStateAll(false);
            }
        }

        /// <summary>
        /// Turns all keys on or off
        /// </summary>
        /// <param name="state">Turn key on or off (True/False)</param>
        public void SetStateAll(bool state)
        {
            if (state)
            {
                for (int i = 0; i < NUMKEYS; i++)
                {
                    frame[FRAMEONOFF, i] = 1;
                }
            }
            else
            {
                for (int i = 0; i < NUMKEYS; i++)
                {
                    frame[FRAMEONOFF, i] = 0;
                }
            }

        }
        #endregion

    }

    /// <summary>
    /// Class for the Roccat RYOS Pro (non FX)
    /// </summary>
    public class RYOSPRO : DLLImport, IFrameModell
    {
        private byte layout;
        private byte[] frame;

        #region CONSTRUCTOR
        public RYOSPRO()
        {
            initFrame();
        }

        public RYOSPRO(LAYOUT layout)
        {
            this.layout = Convert.ToByte(layout);
            initFrame();
        }

        public RYOSPRO (byte layout)
        {
            this.layout = layout;
            initFrame();
        }
        #endregion

        #region INIT
        /// <summary>
        /// Establishes connection to Roccat RYOS MK PRO
        /// </summary>
        /// <returns>False: when connection failed / True: when connection has been established</returns>
        public bool InitRYOSMK()
        {
            return init_ryos_talk();
        }

        public void initFrame()
        {
            this.frame = new byte[NUMKEYS];
            for (int i = 0; i < NUMKEYS; i++)
            {
                this.frame[i] = 0x00;
            }
        }

        /// <summary>
        /// Turn on or off SDK-Mode
        /// </summary>
        /// <param name="enable">SDK-Mode (True=ON/False=OFF)</param>
        public void SDKMode(bool enable)
        {
            try
            {
                set_ryos_kb_SDKmode(enable);
            }
            catch (Exception e)
            {
                throw new FailedToEnterSDKModeException("Failed to enter SDK Mode", e);
            }

        }
        #endregion

        #region ALL LED

        /// <summary>
        /// Turns on all LEDS
        /// </summary>
        public void TurnOnAllLEDS()
        {
            turn_on_all_LEDS();
        }

        /// <summary>
        /// Turns off all LEDS
        /// </summary>
        public void TurnOffAllLEDS()
        {
            turn_off_all_LEDS();
        }

        /// <summary>
        /// Lets the whole keyboard blink at 'interval' (in ms) for 'count' times
        /// </summary>
        /// <param name="interval">interval in ms</param>
        /// <param name="count">amount of blinks</param>
        public void AllKeyBlinking(int interval, int count)
        {
            All_Key_Blinking(interval, count);
        }
        #endregion

        #region SetLEDOn
        public void SetLEDOn(byte pos)
        {
            set_LED_on(pos);
        }

        public void SetLEDOn(int pos)
        {
            set_LED_on(Convert.ToByte(pos));
        }

        public void SetLEDOn(KEYSGER pos)
        {
            set_LED_on(Convert.ToByte(pos));
        }

        public void SetLEDOn(KEYSUS pos)
        {
            set_LED_on(Convert.ToByte(pos));
        }
        #endregion

        #region SetLEDOff
        public void SetLEDOff(byte pos)
        {
            set_LED_off(pos);
        }

        public void SetLEDOff(int pos)
        {
            set_LED_off(Convert.ToByte(pos));
        }

        public void SetLEDOff(KEYSGER pos)
        {
            set_LED_off(Convert.ToByte(pos));
        }

        public void SetLEDOff(KEYSUS pos)
        {
            set_LED_off(Convert.ToByte(pos));
        }
        #endregion

        #region FRAMEMODEL
        /// <summary>
        /// Sends frame to Roccat RYOS MK PRO Keyboard
        /// </summary>
        public void SendFrame()
        {
            Set_all_LEDS(frame, layout);
        }
        /// <summary>
        /// Sets a Keystate in the Frame
        /// </summary>
        /// <param name="key">Index of the Key</param>
        /// <param name="state">True: ON / False: OFF</param>
        public void SetKey(int key, bool state)
        {
            if (state)
            {
                frame[key] = 0x01;
            }           
            else
            {
                frame[key] = 0x00;
            }
        }

        /// <summary>
        /// Sets a Keystate in the Frame
        /// </summary>
        /// <param name="key">Key from Layout</param>
        /// <param name="state">True: ON / False: OFF</param>
        public void SetKey(KEYSGER key, bool state)
        {
            if (state)
            {
                frame[Convert.ToInt32(key)] = 0x01;
            }
            else
            {
                frame[Convert.ToInt32(key)] = 0x00;
            }
        }

        /// <summary>
        /// Sets a Keystate in the Frame
        /// </summary>
        /// <param name="key">Key from Layout</param>
        /// <param name="state">True: ON / False: OFF</param>
        public void SetKey(KEYSUS key, bool state)
        {
            if (state)
            {
                frame[Convert.ToInt32(key)] = 0x01;
            }
            else
            {
                frame[Convert.ToInt32(key)] = 0x00;
            }
        }

        /// <summary>
        /// Sets a Keystate in the Frame
        /// </summary>
        /// <param name="key">Key from Layout</param>
        /// <param name="state">State from KEYSTATE</param>
        public void SetKey(KEYSGER key, KEYSTATE state)
        {
            frame[Convert.ToInt32(key)] = Convert.ToByte(state);
        }

        /// <summary>
        /// Sets a Keystate in the Frame
        /// </summary>
        /// <param name="key">Key from Layout</param>
        /// <param name="state">State from KEYSTATE</param>
        public void SetKey(KEYSUS key, KEYSTATE state)
        {           
            frame[Convert.ToInt32(key)] = Convert.ToByte(state);             
        }

        /// <summary>
        /// Sets a Keystate in the Frame
        /// </summary>
        /// <param name="key">Index of the Key</param>
        /// <param name="state">State from KEYSTATE</param>
        public void SetKey(int key, KEYSTATE state)
        {
            frame[key] = Convert.ToByte(state);
        }

        /// <summary>
        /// Returns a copy of the current Framedata
        /// </summary>
        /// <returns>copy of Frame as bool[]</returns>
        public bool[] GetFrameBool()
        {
            bool[] result = new bool[NUMKEYS];
            for (int i = 0; i < NUMKEYS; i++)
            {
                if (frame[i] == 0x00)
                {
                    result[i] = false;
                }
                else
                {
                    result[i] = true;
                }                
            }
            return result;
        }

        /// <summary>
        /// Returns a copy of the current Framedata
        /// </summary>
        /// <returns>copy of Frame as int[]</returns>
        /// 
        public int[] GetFrameInteger()
        {
            int[] result = new int[NUMKEYS];
            for (int i = 0; i < NUMKEYS; i++)
            {
                if (frame[i] == 0x00)
                {
                    result[i] = 0;
                }
                else
                {
                    result[i] = 1;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a copy of the current Framedata
        /// </summary>
        /// <returns>copy of Frame as byte[]</returns>
        public byte[] GetFrameByte()
        {
            byte[] result = new byte[NUMKEYS];
            for (int i = 0; i < NUMKEYS; i++)
            {
                if (frame[i] == 0x00)
                {
                    result[i] = 0x00;
                }
                else
                {
                    result[i] = 0x01;
                }
            }
            return result;
        }

        /// <summary>
        /// Sets the current Frame
        /// </summary>
        /// <param name="frame">Frame-Data as bool[keypos] True: ON / False: OFF</param>
        public void SetFrame(bool[] frame)
        {
            if (frame.Length > NUMKEYS)
            {
                throw new InvalidFrameException("The Frame is to big. Size: "+frame.Length+" (Expected: "+NUMKEYS+")");
            }
            else
            {
                bool frameok = true;
                for (int i = 0; i < NUMKEYS; i++)
                {                    
                    if((frame[i] == true) || (frame[i] == false))
                    {
                        
                    }
                    else
                    {
                        frameok = false;
                    }
                }

                if (frameok)
                {
                    for (int i = 0; i < NUMKEYS; i++)
                    {
                        if (frame[i])
                        {
                            this.frame[i] = 0x01;
                        }
                        else
                        {
                            this.frame[i] = 0x00;
                        }                       
                    }
                }
                else
                {
                    throw new InvalidFrameException("The Frame contains unexpected values");
                }
            }
        }

        /// <summary>
        /// Sets the current Frame
        /// </summary>
        /// <param name="frame">Frame-Data as int[keypos] 1: ON / 0: OFF</param>
        public void SetFrame(int[] frame)
        {
            if (frame.Length > NUMKEYS)
            {
                throw new InvalidFrameException("The Frame is to big. Size: " + frame.Length + " (Expected: " + NUMKEYS + ")");
            }
            else
            {
                bool frameok = true;
                for (int i = 0; i < NUMKEYS; i++)
                {
                    if ((frame[i] == 1) || (frame[i] == 0))
                    {
                        
                    }
                    else
                    {
                        frameok = false;
                    }
                }

                if (frameok)
                {
                    for (int i = 0; i < NUMKEYS; i++)
                    {
                        if (frame[i] == 1)
                        {
                            this.frame[i] = 0x01;
                        }
                        else
                        {
                            this.frame[i] = 0x00;
                        }
                    }
                }
                else
                {
                    throw new InvalidFrameException("The Frame contains unexpected values");
                }
            }
        }

        /// <summary>
        /// Sets the current Frame
        /// </summary>
        /// <param name="frame">Frame-Data as byte[keypos] 0x01: ON / 0x00: OFF</param>
        public void SetFrame(byte[] frame)
        {
            if (frame.Length > NUMKEYS)
            {
                throw new InvalidFrameException("The Frame is to big. Size: " + frame.Length + " (Expected: " + NUMKEYS + ")");
            }
            else
            {
                bool frameok = true;
                for (int i = 0; i < NUMKEYS; i++)
                {
                    if ((frame[i] == 0x01) || (frame[i] == 0x00))
                    {

                    }
                    else
                    {
                        frameok = false;
                    }
                }

                if (frameok)
                {
                    for (int i = 0; i < NUMKEYS; i++)
                    {
                        if (frame[i] == 0x01)
                        {
                            this.frame[i] = 0x01;
                        }
                        else
                        {
                            this.frame[i] = 0x00;
                        }
                    }
                }
                else
                {
                    throw new InvalidFrameException("The Frame contains unexpected values");
                }
            }
        }     
        #endregion

    }
    #endregion

    #region RoccatTalk
    /// <summary>
    /// Class for Roccat TALKFS [NOT FUNCTIONAL]
    /// </summary>
    public class TALKFX : DLLImport
    {
        public TALKFX()
        {

        }

        public void SetLEDRGB(ZONE zone, EFFECT effect, SPEED speed, int colorR, int colorG, int colorB)
        {
            Set_LED_RGB(Convert.ToByte(zone), Convert.ToByte(effect), Convert.ToByte(speed), Convert.ToByte(colorR), Convert.ToByte(colorG), Convert.ToByte(colorB));
        }
        public void SetLEDRGB(byte zone, byte effect, byte speed, int colorR, int colorG, int colorB)
        {
            Set_LED_RGB(zone, effect, speed, Convert.ToByte(colorR), Convert.ToByte(colorG), Convert.ToByte(colorB));
        }
        public void SetLEDRGB(byte zone,byte effect, byte speed, byte colorR, byte colorG, byte colorB)
        {
            Set_LED_RGB(zone, effect,speed, colorR, colorG,colorB);
        }
        public void RestoreLED()
        {
            RestoreLEDRGB();
        }
        
    }
    #endregion

    #region BASECLASSES
    /// <summary>
    /// Defines the base Functions for the Frame Modell functionality
    /// </summary>
    public interface IFrameModell
    {
        /// <summary>
        /// Sends frame to keyboard
        /// </summary>
        void SendFrame();
        /// <summary>
        /// prepares the Frame
        /// </summary>
        void initFrame();
    }

    /// <summary>
    /// Imports all functions from the C++ DLL
    /// </summary>
    public abstract class DLLImport
    {
        protected const int NUMKEYS = 120;

        private const string DLL = "RoccatTalk2019.dll";
        private const CallingConvention CC = CallingConvention.StdCall;

        //DLL Imports
        [DllImport(DLL, CallingConvention = CC)]
        protected static extern bool init_ryos_talk();

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void RestoreLEDRGB();

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void set_ryos_kb_SDKmode(bool sdkmode);

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void turn_off_all_LEDS();

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void turn_on_all_LEDS();

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void set_LED_on(byte ucPos);

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void set_LED_off(byte ucPos);

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void Set_all_LEDS(byte[] ucLED, byte country);

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void All_Key_Blinking(int DelayTime, int LoopTimes);

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void Set_LED_RGB(byte bZone, byte bEffect, byte bSpeed, byte colorR, byte colorG, byte colorB);

        [DllImport(DLL, CallingConvention = CC)]
        protected static extern void Set_all_LEDSFX(byte[] frame_dataOnOff, byte[] frame_dataR, byte[] frame_dataG, byte[] frame_dataB, byte layout);

        [DllImport(DLL, CallingConvention = CC)]
        public static extern void Set_Led_RGB(byte bEffect, byte bSpeed, byte colorR, byte colorG, byte colorB);

        public DLLImport()
        {

        }
    }
    #endregion

    #region EXCEPTIONS
    class FailedToSendFrameException : Exception
    {
        public readonly byte[,] FRAME;

        public FailedToSendFrameException(byte[,] FRAME)
        {
            this.FRAME = FRAME;
        }

        public FailedToSendFrameException(byte[,] FRAME, string message) : base(message)
        {
            this.FRAME = FRAME;
        }

        public FailedToSendFrameException(byte[,] FRAME, string message, Exception inner) : base(message, inner)
        {
            this.FRAME = FRAME;
        }
    }

    class FailedToEnterSDKModeException : Exception
    {
        public FailedToEnterSDKModeException()
        {

        }

        public FailedToEnterSDKModeException(string message) : base(message)
        {

        }

        public FailedToEnterSDKModeException(string message, Exception inner) : base(message, inner)
        {

        }
    }

    class ColorValueOutOfRangeException : Exception
    {
        public readonly int COLORVALUE;

        public ColorValueOutOfRangeException(int COLORVALUE)
        {
            this.COLORVALUE = COLORVALUE;
        }

        public ColorValueOutOfRangeException(int COLORVALUE, string message) : base(message)
        {
            this.COLORVALUE = COLORVALUE;
        }

        public ColorValueOutOfRangeException(int COLORVALUE, string message, Exception inner) : base(message, inner)
        {
            this.COLORVALUE = COLORVALUE;
        }
    }

    class KeyIndexOutOfRangeException : Exception
    {
        public readonly int INDEX;

        public KeyIndexOutOfRangeException(int INDEX)
        {
            this.INDEX = INDEX;
        }

        public KeyIndexOutOfRangeException(int INDEX, string message) : base(message)
        {
            this.INDEX = INDEX;
        }

        public KeyIndexOutOfRangeException(int INDEX, string message, Exception inner) : base(message, inner)
        {
            this.INDEX = INDEX;
        }
    }

    class InvalidFrameException : Exception
    {
        public InvalidFrameException()
        {

        }
        public InvalidFrameException(string message) : base(message)
        {

        }
        public InvalidFrameException(string message, Exception inner) : base(message, inner)
        {

        }

    }
    #endregion

    #region ENUM
    /// <summary>
    /// Known Keyboard Layout-Codes
    /// </summary>
    public enum LAYOUT
    {
        GER = 0x00, ES = 0x00, FR = 0x00, HU = 0x00, IT = 0x00, LA = 0x00, NO = 0x00, PT = 0x00, TR = 0x00, SZ = 0x00, CZ = 0x00, BE = 0x00, CH = 0x00, UK = 0x00,
        US = 0x01, RU = 0x01, KR = 0x01, JP = 0x02,
        QWERTZ = 0x10,
        QWERTY = 0x11,
        AZERTY = 0x12
    }

    /// <summary>
    /// States for Keys
    /// </summary>
    public enum KEYSTATE
    {
        ON = 0x01,
        OFF = 0x00
    }

    /// <summary>
    /// Talk FX Zones
    /// </summary>
    public enum ZONE
    {
        AMBIENT = 0x00,
        EVENT = 0x01
    }

    /// <summary>
    /// Talk FX Effects
    /// </summary>
    public enum EFFECT
    {
        OFF = 0x00,
        ON = 0x01,
        BLINKING = 0x02,
        BREATING = 0x03,
        HEARTBEAT = 0x04
    }

    /// <summary>
    /// Talk FX Effect Speeds
    /// </summary>
    public enum SPEED
    {
        NOCHANGE = 0x00,
        SLOW = 0x01,
        NORMAL = 0x02,
        FAST = 0x03
    }

    /// <summary>
    /// Predefined Key-Indexes for German Keyboards
    /// </summary>
    public enum KEYSGER
    {
        ESC, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, DRUCK, ROLL, PAUSE,
        M1, ZIRKUMFLEX, K1, K2, K3, K4, K5, K6, K7, K8, K9, K0, SCHARFS, AKUT, LOESCHEN, EINFG, POS1, BILDAUF, NUM, NDIVISION, NMULTIPLIKATION, NMINUS,
        M2, TAB, KQ, KW, KE, KR, KT, KZ, KU, KI, KO, KP, KUE, KADD, ENTER, ENTF, ENDE, BILDAB, N7, N8, N9, NPLUS,
        M3, CAPS, KA, KS, KD, KF, KG, KH, KJ, KK, KL, KOE, KAE, KRAUTE, N4, N5, N6,
        M4, SHIFTL, KVERGLEICH, KY, KX, KC, KV, KB, KN, KM, KKOMA, KPUNKT, KBINDESTRICK, SHIFTR, PFEILAUF, N1, N2, N3, NENTER,
        M5, STRGL, WIN, ALT, LEER, ALTGR, FN, ANDWENDUNG, STRGR, PFEILLINKS, PFEILAB, PFEILRECHTS, N0, NKOMA
    }

    /// <summary>
    /// Predefined Key-Indexes for US Keyboards (not yet correct)
    /// </summary>
    public enum KEYSUS
    {
        ESC, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, PRINT, ROLL, PAUSE,
        M1, ZIRKUM, K1, K2, K3, K4, K5, K6, K7, K8, K9, K0, SHARPS, QUOTE, BACKSPACE, PASTE, HOME, PUP, NUM, NDIV, NMUL, NMIN,
        M2, TAB, KQ, KW, KE, KR, KT, KZ, KU, KI, KO, KP, KUE, KADD, ENTER, DEL, END, PDO, N7, N8, N9, NADD,
        M3, CAPS, KA, KS, KD, KF, KG, KH, KJ, KK, KL, KOE, KAE, KTAG, N4, N5, N6,
        M4, SHIFTL, KARROW, KY, KX, KC, KV, KB, KN, KM, KCOMA, KDOT, KLINE, SHIFTR, ARRUP, N1, N2, N3, NENTER,
        M5, CTRLL, WIN, ALT, SPACE, ALTGR, FN, MENU, CTRLR, ARRLEFT, ARRDOWN, ARRRIGHT, N0, NCOMA
    }
    #endregion
}
